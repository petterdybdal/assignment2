# Assignment 2

## Description
This is an SQL Database-project for the second assignment in the Noroff fullstack-.NET course.

The assignment is split up into two parts: in part A we were tasked to write 9 queries to build a database called SuperheroDb, and solve different SQL-related tasks related to this. Part B consisted of manipulating SQL Server data in Visual Studio using SQL Client. The database manipulated here was a pre-built database provided to us called "Chinook".

## Installation
#### Part A
For part A you can simply run the queries found in the folder `SqlScripts\` in Microsoft SQL Server Management Studio. No database is provided, since the queries build the database and executes the different tasks in order.

#### Part B
Clone the project and simply run the main function. In order to connect the database to the program in part B, you need to provide the DataSource of the database to the ConnectionHelper class.

This can be done by editing line 15 in the file `\AppendixB\Repository\ConnectionHelper.cs`.

```cs
connectStringBuilder.DataSource = "YOUR_DATA_SOURCE_HERE";
```

Replace the string `YOUR_DATA_SOURCE_HERE` with your data source string found in SSMS.

The database used in the project - "Chinook" is not provided in this repository.

The main function contains some commented-out code to execute each of the tasks assigned in order. Simply comment these out one by one to run them, or feel free to edit or add to the code in the main method to get different results.

## Tasks
### Part A (SQL scripts to create database)
All the tasks from part A can be found in the folder `SqlScripts`. The folder contains 9 .sql files, which contains our SQL-Queries to solve the corresponding tasks in appendix A.
##### Task 1
> Create a script called 01_dbCreate.sql that contains a statement to create the database

Our SQL Query to solve this task can be found in `\SqlScripts\01_dbCreate.sql`

##### Task 2
> Create a script called 02_tableCreate.sql that contains statements to create each of these tables and
> setup their primary keys.

Our SQL Query to solve this task can be found in `\SqlScripts\02_tableCreate.sql`

##### Task 3
> Create a script called 03_relationshipSuperheroAssistant.sql that contains statements to ALTER any
> tables needed to add the foreign key and setup the constraint to configure the described relationship between
> Superhero and assistant. 

Our SQL Query to solve this task can be found in `\SqlScripts\03_relationshipSuperheroAssistant.sql`

##### Task 4
> Create a script called 04_relationshipSuperheroPower.sql that contains statements to create the linking
> table. This script should contain any ALTER statements needed to set up the foreign key constraints between the linking
> tables and the Superhero and Power tables.

Our SQL Query to solve this task can be found in `\SqlScripts\04_relationshipSuperheroPower.sql`

##### Task 5
> Create a script called 05_insertSuperheroes.sql that inserts three new superheroes into the database.

Our SQL Query to solve this task can be found in `\SqlScripts\05_insertSuperheroes.sql`

##### Task 6
> Create a script called 06_insertAssistants.sql that inserts three assistants, decide on which superheroes
> these can assist.

Our SQL Query to solve this task can be found in `\SqlScripts\06_insertAssistants.sql`

##### Task 7
> Create a script called 07_powers.sql that inserts four powers. Then the script needs to give the
> superheroes powers. Try have one superhero with multiple powers and one power that is used by multiple superheroes,
> to demonstrate the many-to-many.

Our SQL Query to solve this task can be found in `\SqlScripts\07_powers.sql`

##### Task 8
> Create a script called 08_updateSuperhero.sql where you can update a superheroes name. Pick any
> superhero to do this with.

Our SQL Query to solve this task can be found in `\SqlScripts\08_updateSuperhero.sql`

##### Task 9
> Create a script called 09_deleteAssistant.sql where you can delete any assistant. You can delete that
> assistant by name (his name must be unique to avoid deleing multiple assistants), this is done to ease working with
> autoincremented numbers – my PC may skip a number or two.

Our SQL Query to solve this task can be found in `\SqlScripts\09_deleteAssistant.sql`

### ER Diagram for Part A

![ER Diagram for Part A](https://gitlab.com/petterdybdal/assignment2/-/raw/main/ER_Diagrams/Superheroes_ER.PNG)

### Part B (Reading data with SQL Client)
For this assignment we have added all the methods to solve to tasks described below line by line in the main method. These are commented out in the repository in order to not run them all at once. To run the different methods, simply remove the comment in front of the method you want to run.
##### Task 1 
> Read all the customers in the database, this should display their: Id, first name, last name, country, postal code,
> phone number and email.

This can be tested and ran by calling the method `PrintAllRecords(repository);` in main function of the program.

##### Task 2
> Read a specific customer from the database (by Id), should display everything listed in the above point.

This can be tested and ran by calling the method `PrintCustomer(repository.GetCustomerById(60));` in main function of the program.
The parameter `60` can be changed to get a different customer by id, and is only there for testing purposes.

##### Task 3
> Read a specific customer by name. HINT: LIKE keyword can help for partial matches.

This can be tested and ran by calling the method `PrintCustomers(repository.GetCustomerByName("a"));` in main function of the program.
The parameter `"a"` can be changed to get a different customer by name, and is only there for testing purposes.

##### Task 4
> Return a page of customers from the database. This should take in limit and offset as parameters and make use
> of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above
> should be reused.

This can be tested and ran by calling the method `PrintCustomers(repository.GetPageOfCustomers(10, 5));` in main function of the program.
The parameters `10` and `5` can be changed to get a different limit and offset for the pagination, and is only there for testing purposes.

##### Task 5
> Add a new customer to the database. You also need to add only the fields listed above (our customer object)

This can be tested and ran by calling the method `InsertRecord(repository);` in main function of the program.
The object to be inserted is created inside the method `InsertRecord(repository)` for testing purposes. Feel free to change this to see different results.

##### Task 6
> Update an existing customer.

This can be tested and ran by calling the method `EditRecord(repository);` in main function of the program.
The object to be edited and its fields is created inside the method `EditRecord(repository)` for testing purposes. Feel free to change this to see different results.

##### Task 7
> Return the number of customers in each country, ordered descending (high to low). i.e. USA: 13, … 

This can be tested and ran by calling the method `PrintCountryCount(repository.GetCustomerCountryCount());` in main function of the program.

##### Task 8
> Customers who are the highest spenders (total in invoice table is the largest), ordered descending.

This can be tested and ran by calling the method `PrintCustomerSpenders(repository.GetHighestSpenders());` in main function of the program.

##### Task 9
> For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context
> means the genre that corresponds to the most tracks from invoices associated to that customer.

This can be tested and ran by calling the method `PrintCustomerGenres(repository.GetMostPopularGenre(customer));` in main function of the program.
We created a customer object to pass into the method inside the main method with CustomerId = 12. Feel free to change this to test for different customers.

### ER Diagram for Part B

![ER Diagram for Part B](https://gitlab.com/petterdybdal/assignment2/-/raw/main/ER_Diagrams/Chinook_ER.PNG)

## Contributors
This assignment as a whole was developed by Petter Dybdal ([petterdybdal](https://gitlab.com/petterdybdal)) and Tore Kamsvåg ([Torekamsvag](https://gitlab.com/Torekamsvag)).