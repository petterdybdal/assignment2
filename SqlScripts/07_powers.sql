USE SuperheroesDb

INSERT INTO Power (Name, Description)
VALUES ('Laser eyes', 'Ability to shoot powerful beams from eyes')

INSERT INTO Power (Name, Description)
VALUES ('Chrono-manipulation', 'Control time')

INSERT INTO Power (Name, Description)
VALUES ('Telekenesis', 'Ability to move objects the mind')

INSERT INTO Power (Name, Description)
VALUES ('Elemental Fusion', 'The ability to control and manipulate elements')

INSERT INTO SuperheroPower (SuperheroID, PowerID)
VALUES (1, 1)

INSERT INTO SuperheroPower (SuperheroID, PowerID)
VALUES (2, 1)

INSERT INTO SuperheroPower (SuperheroID, PowerID)
VALUES (1, 2)

INSERT INTO SuperheroPower (SuperheroID, PowerID)
VALUES (3, 3)

INSERT INTO SuperheroPower (SuperheroID, PowerID)
VALUES (3, 4)