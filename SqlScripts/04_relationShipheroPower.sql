USE SuperheroesDb

CREATE TABLE SuperheroPower (
    ID int IDENTITY(1, 1) PRIMARY KEY,
	SuperheroID int FOREIGN KEY REFERENCES Superhero(ID),
	PowerID int FOREIGN KEY REFERENCES Power(ID)
)