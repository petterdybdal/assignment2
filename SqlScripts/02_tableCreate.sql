USE SuperheroesDb

CREATE TABLE Superhero (
	ID int IDENTITY(1, 1) PRIMARY KEY,
	Name nchar(50),
	Alias nchar(50),
	Origin nchar(50)
)

CREATE TABLE Assistant (
	ID int IDENTITY(1, 1) PRIMARY KEY,
	Name nchar(50)
)

CREATE TABLE Power (
	ID int IDENTITY(1, 1) PRIMARY KEY,
	Name nchar(50),
	Description nchar(50)
)
