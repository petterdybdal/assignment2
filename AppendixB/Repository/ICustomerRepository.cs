﻿using Assignment2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Repository
{
    public interface ICustomerRepository
    {
        List<Customer> GetAllCustomers();
        Customer GetCustomerById(int customerId);
        List<Customer> GetCustomerByName(string customerName);
        List<Customer> GetPageOfCustomers(int limit, int offset);
        bool AddNewCustomer(Customer customer);
        bool UpdateCustomer(Customer customer);
        List<CustomerCountry> GetCustomerCountryCount();
        List<CustomerSpender> GetHighestSpenders();
        List<CustomerGenre> GetMostPopularGenre(Customer customer);
    }
}
