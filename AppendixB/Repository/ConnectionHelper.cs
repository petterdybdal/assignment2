﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    public class ConnectionHelper
    {
        /// <summary>
        ///  The method creates a connection with the Chinook database in SQL.
        /// </summary>
        /// <returns> The method returns a connection string, which can be used to open and close a connection. </returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "YOUR_DATA_SOURCE_HERE";
            connectStringBuilder.InitialCatalog = "Chinook";
            connectStringBuilder.IntegratedSecurity = true;
            connectStringBuilder.Encrypt = false;
            return connectStringBuilder.ConnectionString;
        }
    }
}
