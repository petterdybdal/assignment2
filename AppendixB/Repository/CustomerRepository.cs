﻿using Assignment2.Models;
using Microsoft.Data.SqlClient;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace Assignment2.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        // Task 1
        /// <summary>
        /// A method to get all of the customers.
        /// </summary>
        /// <returns>The method returns a list of customer objects, which contains values for CustomerId, FirstName,
        /// LastName, Country, PostalCode, Phone and Email for all customers in the database.</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> ret = new List<Customer>();
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader[1] as string;
                                temp.LastName = reader[2] as string;
                                temp.Country = reader[3] as string;
                                temp.PostalCode = reader[4] as string;
                                temp.Phone = reader[5] as string;
                                temp.Email = reader[6] as string;

                                ret.Add(temp);
                            }
                        }
                    }    
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return ret;

        }

        // Task 2
        /// <summary>
        /// A method to get a specific customer by their ID.
        /// </summary>
        /// <param name="customerId"> Takes in the customerId as a parameter.</param>
        /// <returns> The method returns a Customer type object, which contains values for the CustomerId,
        /// FirstName, LastName, Country, PostalCode, Phone, and Email.</returns>
        public Customer GetCustomerById(int customerId)
        {
            Customer temp = new Customer();
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customerId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {   
                            
                            while (reader.Read())
                            {
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader[1] as string;
                                temp.LastName = reader[2] as string;
                                temp.Country = reader[3] as string;
                                temp.PostalCode = reader[4] as string;
                                temp.Phone = reader[5] as string;
                                temp.Email = reader[6] as string;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return temp;
        }

        // Task 3
        /// <summary>
        /// A method to retrieve a list of specific customer(s) by their name.
        /// </summary>
        /// <param name="customer"> The function takes in a customer object as a parameter.</param>
        /// <returns>The method returns a list of customer objects, which contains values for CustomerId, FirstName,
        /// LastName, Country, PostalCode, Phone and Email. The method takes in a list to account for partial matches.</returns>
        public List<Customer> GetCustomerByName(string customer)   
        {
            List<Customer> ret = new List<Customer>();
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @FirstName";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", "%" + customer + "%");
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader[1] as string;
                                temp.LastName = reader[2] as string;
                                temp.Country = reader[3] as string;
                                temp.PostalCode = reader[4] as string;
                                temp.Phone = reader[5] as string;
                                temp.Email = reader[6] as string;

                                ret.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return ret;
        }

        // Task 4
        /// <summary>
        /// A method to retrieve a subset of customers from the database.
        /// </summary>
        /// <param name="limit">An integer which determines the number of rows to be returned.</param>
        /// <param name="offset">An integer which determines the number of rows to skip. (The output will start from offset row +1).</param>
        /// <returns>The method returns a list of Customer type objects which contains values for 
        /// CustomerId, FirstName, LastName, Country, PostalCode, Phone, and Email.</returns>
        public List<Customer> GetPageOfCustomers(int limit, int offset)
        {
            List<Customer> ret = new List<Customer>();
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
                         FROM Customer ORDER BY CustomerId 
                         OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader[1] as string;
                                temp.LastName = reader[2] as string;
                                temp.Country = reader[3] as string;
                                temp.PostalCode = reader[4] as string;
                                temp.Phone = reader[5] as string;
                                temp.Email = reader[6] as string;

                                ret.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return ret;

        }

        // Task 5
        /// <summary>
        /// A method to add a new customer to the database.
        /// </summary>
        /// <param name="customer">The function takes in a customer object, which is a "Customer" type.</param>
        /// <returns>The method returns true if a customer is sucessfully added to the database. 
        /// An SqlException will be thrown if the try block is non-executable and returns false.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = @"INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) 
                         Values(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }

        // Task 6
        /// <summary>
        /// A method to update some information about an existing customer in the database.
        /// </summary>
        /// <param name="customer">The method takes in a customer object.</param>
        /// <returns>The method returns a boolean value, which returns true if a customer is successfully updated in the database.
        /// An SqlException will be thrown if the try block is non-executable and returns false.</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = @"UPDATE Customer 
                         SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email
                         WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }

        // Task 7
        /// <summary>
        /// A method to get the number of customers from each country.
        /// </summary>
        /// <returns>The method returns a list of customers from each country, and is ordered from highest to lowest.
        /// The list contains values for the name of the country and also the number of customers in each country.</returns>
        public List<CustomerCountry> GetCustomerCountryCount()
        {
            List<CustomerCountry> ret = new List<CustomerCountry>();
            string sql = @"SELECT Country, COUNT(Country) FROM Customer GROUP BY Country ORDER BY COUNT(Country) DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry temp = new CustomerCountry();
                                temp.CountryName = reader[0] as string;
                                temp.CountryCount = reader.GetInt32(1);

                                ret.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return ret;
        }

        // Task 8
        /// <summary>
        /// A method to retrieve the customers which has the highest total invoice amount.
        /// </summary>
        /// <returns>The method returns a list of customerSpender objects which containts values for 
        /// CustomerId, FirstName and their total invoice amount (Total).</returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> ret = new List<CustomerSpender>();
            string sql = @"SELECT Invoice.CustomerId, Customer.FirstName, SUM(Total) FROM Invoice
                         JOIN Customer ON Customer.CustomerId = Invoice.CustomerId
                         GROUP BY Invoice.CustomerId, Customer.FirstName
                         ORDER BY SUM(Total) DESC";            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.CustomerName = reader[1] as string;
                                temp.Total = Convert.ToDouble(reader[2].ToString());
                                ret.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return ret;
        }
        
        // Task 9
        /// <summary>
        /// A method which retrieves the number of tracks in each genre for a given customer.
        /// </summary>
        /// <param name="customer">The method takes in a customer object.</param>
        /// <returns>The method outputs a list of CustomerGenre objects which contains values for GenreName and GenreCount.</returns>
        public List<CustomerGenre> GetMostPopularGenre(Customer customer)
        {
            List<CustomerGenre> ret = new List<CustomerGenre>();
            string sql = @"SELECT Genre.Name, COUNT(Genre.Name) FROM Invoice
                         JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId
                         JOIN Track ON Track.TrackId = InvoiceLine.TrackId
                         JOIN Genre ON Genre.GenreId = Track.GenreId
                         WHERE CustomerId = @CustomerId
                         GROUP BY Genre.Name
                         ORDER BY COUNT(Genre.Name) DESC";
            try
            { 
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.GenreName = reader[0] as string;
                                temp.GenreCount = reader.GetInt32(1).ToString();
                                ret.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return ret;
        }
    }
}
