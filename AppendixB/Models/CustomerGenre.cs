﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Models
{
    public class CustomerGenre
    {
        public string InvoiceId { get; set; }
        public string TrackId { get; set; }
        public string GenreName { get; set; }
        public string GenreCount { get; set; }
    }
}
