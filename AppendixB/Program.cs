﻿using Assignment2.Models;
using Assignment2.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace Assignment2
{
    internal class Program
    {
        static void Main(string[] args)
        {   
            ICustomerRepository repository = new CustomerRepository();
            //PrintAllRecords(repository);                              //Task 1

            //PrintCustomer(repository.GetCustomerById(60));            //Task 2

            //PrintCustomers(repository.GetCustomerByName("a"));        //Task 3

            //PrintCustomers(repository.GetPageOfCustomers(10, 5));     //Task 4

            //InsertRecord(repository);                                 //Task 5

            //EditRecord(repository);                                   //Task 6

            //PrintCountryCount(repository.GetCustomerCountryCount());  //Task 7

            //PrintCustomerSpenders(repository.GetHighestSpenders());   //Task 8

            /*                                                          //Task 9
             * Customer customer = new Customer                          
            //{
            //    CustomerId = 12
            //};
            PrintCustomerGenres(repository.GetMostPopularGenre(customer));
            */
            Console.ReadLine();
        }


        /// <summary>
        /// Method for printing all records from the database.
        /// </summary>
        /// <param name="repository">Repository parameter which contains methods from the ICustomerRepository interface.</param>
        static void PrintAllRecords(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        /// <summary>
        /// Method for inserting a new record to the database. The method takes in a new customer object which will need to be specified.
        /// </summary>
        /// <param name="repository">Takes in a repository parameter which contains methods from the ICustomerRepository interface.</param>
        static void InsertRecord(ICustomerRepository repository)
        {
            Customer customer = new Customer()
            {   
                FirstName = "Bjarne",
                LastName = "Brøndbo",
                Country = "Norway",
                PostalCode = "7800",
                Phone = "+47 954 92 035",
                Email = "Bjarne@no.experis.com"
            };
            if (repository.AddNewCustomer(customer))
            {
                Console.WriteLine("Successfully inserted record");
            }
        }

        /// <summary>
        /// Method for printing all customers.
        /// </summary>
        /// <param name="customers">Takes in a customers parameter which is a collection of the Customer type class.</param>
        static void PrintCustomers (IEnumerable<Customer> customers) 
        { 
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        /// <summary>
        /// Method for printing a single customer from the database. 
        /// The CustomerId, FirstName, LastName, Country, PostalCode, Phone and Email will be displayed.
        /// </summary>
        /// <param name="customer">Takes in a customer parameter of type Customer class.</param>
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"{customer.CustomerId},{customer.FirstName},{customer.LastName},{customer.Country},{customer.PostalCode},{customer.Phone},{customer.Email}");
        }

        /// <summary>
        /// Method for updating an existing record in the database.
        /// </summary>
        /// <param name="repository"></param>
        static void EditRecord(ICustomerRepository repository)
        {
            Customer customer = new Customer()
            {   
                CustomerId = 32,
                FirstName = "Bob Kåre",
                LastName = "Brattebø",
                Country = "Sweden",
                PostalCode = "1337",
                Phone = "+46 954 92 035",
                Email = "BobKaare@se.experis.com"
            };
            if (repository.UpdateCustomer(customer))
            {
                Console.WriteLine("Successfully updated record");
            }
        }

        /// <summary>
        /// Method for printing the name of a country and the corresponding number of customers in a country.
        /// </summary>
        /// <param name="customerCountry">Parameter which is a collection of the customerCountry type.</param>
        static void PrintCountryCount(IEnumerable<CustomerCountry> customerCountry)
        {
            foreach (CustomerCountry entry in customerCountry)
            {
                Console.WriteLine($"{entry.CountryName}: {entry.CountryCount}");
            }
        }
        /// <summary>
        /// A method for printing the customers with the highest total invoice from the database.
        /// </summary>
        /// <param name="customerSpender">Parameter which is a collection of the CustomerSpender type.</param>
        static void PrintCustomerSpenders(IEnumerable<CustomerSpender> customerSpender)
        {
            foreach (CustomerSpender entry in customerSpender)
            {
                Console.WriteLine($"CustomerId: {entry.CustomerId} Firstname: {entry.CustomerName} Total: {entry.Total}");
            }
        }

        /// <summary>
        /// A method to print the top genre(s) for a given customer.
        /// </summary>
        /// <param name="customerGenre">Parameter which is a collection of the CustomerGenre type.</param>
        static void PrintCustomerGenres(IEnumerable<CustomerGenre> customerGenre)
        {
            List<string> top = new List<string>();
            int temp = 0;
            foreach (CustomerGenre entry in customerGenre)
            {
                if (int.Parse(entry.GenreCount) >= temp)
                {
                    temp = int.Parse(entry.GenreCount);
                    top.Add(entry.GenreName);
                }
            }
            Console.WriteLine($"Top genre(s) for customer:\n");
            foreach(string entry in top)
            {
                Console.WriteLine(entry);
            }
        }
    }
}
